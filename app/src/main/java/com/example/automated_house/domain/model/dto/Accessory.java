package com.example.automated_house.domain.model.dto;

import java.util.UUID;

public class Accessory {

    private Long id;
    private String name;
    private boolean state;
    private int value;

    public Long getId() {
        return id;
    }

    public void setID(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Accessory(Long id, String name, boolean state, int value) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.value = value;
    }
}
