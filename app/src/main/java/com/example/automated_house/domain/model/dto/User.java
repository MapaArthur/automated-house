package com.example.automated_house.domain.model.dto;

import java.util.UUID;

public class User {

    private Long id;
    private String name;
    private String cpf;
    private String password;

    public User(Long id, String name, String cpf, String password) {
        this.id = id;
        this.name = name;
        this.cpf = cpf;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString(){
        return ("nome:" + this.name +" cpf:" + this.cpf + " password:"+this.password);
    }
}
