package com.example.automated_house.domain.model.enums;

public enum TypeRoom {
    LIVING_ROOM("Sala"),
    BEDROOM("Quarto"),
    BATHROOM("Banheiro"),
    KITCHEN("Cozinha"),
    GARAGE("Garagem"),
    ROOMCUP("Copa"),
    BALCONY("Varanda"),
    SUITE("Suíte"),
    YARD("Quintal");

    private String type;
    TypeRoom(String type) {
        this.type = type;
    }

    public String getType()
    {
        return this.type;
    }
}
