package com.example.automated_house.domain.model.dto;

import java.util.List;
import java.util.UUID;

public class Residence {
    private Long id;
    private String name;
    private List<String> cpfList;
    private List<Room> roomList;
    private List<User> userList;
    private String state;
    private String city;
    private String district;
    private String road;
    private int number;

    public Residence(Long id, String state, String city, String district, String road, int number) {
        this.id = id;
        this.state = state;
        this.city = city;
        this.district = district;
        this.road = road;
        this.number = number;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<String> getCpfList() { return cpfList; }

    public void setCpfList(List<String> cpfList) { this.cpfList = cpfList; }

    public List<Room> getRoomList() { return roomList; }

    public void setRoomList(List<Room> roomList) { this.roomList = roomList; }

    public List<User> getUserList() { return userList; }

    public void setUserList(List<User> userList) { this.userList = userList; }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
