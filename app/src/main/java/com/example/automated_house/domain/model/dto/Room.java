package com.example.automated_house.domain.model.dto;

import com.example.automated_house.domain.model.enums.TypeRoom;

import java.util.List;

public class Room {

    private Long id;
    private String name;
    private List<Accessory> accessoryList;
    private TypeRoom type;

    public Room(Long id, String name, List<Accessory> accessoryList, TypeRoom type) {
        this.id = id;
        this.name = name;
        this.accessoryList = accessoryList;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Accessory> getAccessoryList() {
        return accessoryList;
    }

    public void setAccessoryList(List<Accessory> accessoryList) {
        this.accessoryList = accessoryList;
    }

    public TypeRoom getType() {
        return type;
    }

    public void setType(TypeRoom type) {
        this.type = type;
    }
}
