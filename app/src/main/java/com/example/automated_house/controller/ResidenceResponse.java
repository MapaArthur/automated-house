package com.example.automated_house.controller;

import com.example.automated_house.domain.model.dto.Accessory;
import com.example.automated_house.domain.model.dto.Residence;
import com.example.automated_house.domain.model.dto.Room;

import java.util.List;

public class ResidenceResponse {
    private Residence residence;
    private List<Room> roomList;
    private List<Accessory> accessoryList;

    public Residence getResidence() {
        return residence;
    }

    public void setResidence(Residence residence) {
        this.residence = residence;
    }

    public List<Room> getRoomList() {
        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }

    public List<Accessory> getAccessoryList() {
        return accessoryList;
    }

    public void setAccessoryList(List<Accessory> accessoryList) {
        this.accessoryList = accessoryList;
    }
}
