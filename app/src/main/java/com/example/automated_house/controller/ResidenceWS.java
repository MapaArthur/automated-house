package com.example.automated_house.controller;

import android.os.AsyncTask;

import com.example.automated_house.domain.model.dto.Residence;
import com.example.automated_house.domain.model.dto.User;
import com.google.gson.Gson;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class ResidenceWS extends AsyncTask<Void, Void, ResidenceResponse> {

    private final String cpf;

    public ResidenceWS(String cpf){
        this.cpf = cpf;
    }

    @Override
    protected ResidenceResponse doInBackground(Void... voids) {
        StringBuilder resp = new StringBuilder();

        try {
            URL url = new URL("https://still-inlet-49193.herokuapp.com/api/automatedHouse/" + this.cpf);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setConnectTimeout(10000);
            connection.connect();

            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
                resp.append(scanner.next());
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return new Gson().fromJson(resp.toString(), ResidenceResponse.class);
    }
}
