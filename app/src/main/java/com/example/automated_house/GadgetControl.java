package com.example.automated_house;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Timer;
import java.util.TimerTask;

public class GadgetControl extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gadget_control);
        Switch onOffBtn = findViewById(R.id.onOffBtn);
        EditText edtControl = findViewById(R.id.edtControl);
        TextView labelStatus = findViewById(R.id.labelStatus);
        edtControl.setText("23");

        onOffBtn.setOnClickListener( (View v) -> {
            if(onOffBtn.isChecked()){
                Log.d("GadgetControl", "Enviando comando de ligar para controlador: {idAccessory:airConditionerxx1, state:on}");
                labelStatus.setText("STATUS: LIGADO (23°)");
            } else{
                Log.d("GadgetControl", "Enviando comando de desligar para controlador: {idAccessory:airConditionerxx1, state:off}");
            }
        });

        edtControl.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
//                Log.d("GadgetControl", "entrei after");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0){
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Log.d("GadgetControl", "Enviando comando para controlador: {idAccessory:airConditionerxx1, value:"+edtControl.getText() +"}");
                            labelStatus.setText("STATUS: LIGADO (26°)");
                        }
                    }, 2000);
                }
            }
        });
    }
}