package com.example.automated_house;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SelectGadget extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_gadget);

        //Pega a intent de outra activity
        Intent it = getIntent();
        //Recuperei a string da outra activity
        String resName = it.getStringExtra("resName");
        System.out.println("\n\n resName: "+resName);
        String userName = it.getStringExtra("userName");
        Activity thisActivity = this;

        //pega o cardview pelo click
        CardView cvAr = findViewById(R.id.cvAr);
        cvAr.setOnClickListener( (View v) -> {
            cvAr.setCardBackgroundColor(Color.GRAY);
            Intent itn = new Intent(thisActivity, GadgetControl.class);
            startActivity(itn);
        });
    }
}