package com.example.automated_house;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.automated_house.domain.model.dto.Residence;
import com.example.automated_house.domain.model.dto.User;

public class SelectRoom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_room);

        //Pega a intent de outra activity
        Intent it = getIntent();
        //Recuperei a string da outra activity
        String resName = it.getStringExtra("resName");
        System.out.println("\n\n resName: "+resName);
        String userName = it.getStringExtra("userName");
        TextView txtResName = findViewById(R.id.txtResName);
        TextView txtWelcomeUser = findViewById(R.id.txtWelcomeUser);
        txtResName.setText(resName);
        txtWelcomeUser.setText("Bem vindo "+userName+"!");
        Activity thisActivity = this;

        //pega o cardview pelo click
        CardView cvQuarto = findViewById(R.id.cvQuarto);
        cvQuarto.setOnClickListener( (View v) -> {
            cvQuarto.setCardBackgroundColor(Color.GRAY);
            Intent itn = new Intent(thisActivity, SelectGadget.class);
            startActivity(itn);
        });
    }
}