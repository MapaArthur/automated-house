package com.example.automated_house;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.automated_house.config.Database;
import com.example.automated_house.controller.ResidenceResponse;
import com.example.automated_house.controller.UserWS;
import com.example.automated_house.controller.ResidenceWS;
import com.example.automated_house.domain.model.dto.Residence;
import com.example.automated_house.domain.model.dto.User;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Database db = new Database();

        //Luigi - inicio dev_tela_login
        Button btnLogin = findViewById(R.id.btnEntrar);
        EditText edtCpf = findViewById(R.id.edtCpf);
        MainActivity thisActivity = this;
//        Log.d("GadgetControl", "Enviando comando para controlador: {idAccessory:airConditionerxx1, state:on, value:26}");
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    if(edtCpf.getText()!= null){
                        //User userLogin = new UserWS(edtCpf.getText().toString()).execute().get();
                        //System.out.println("User: "+ userLogin.toString());
//                        Log.d("MainActivity", "User: "+ userLogin.toString());
                        //if(userLogin!=null){
                            //ResidenceResponse resResponse = new ResidenceWS(edtCpf.getText().toString()).execute().get();
                            //Residence res = resResponse.getResidence();
                            //if(res != null){
                                Intent it = new Intent(thisActivity, SelectRoom.class);
                                it.putExtra("resName", "Casa do Luigi");
                                it.putExtra("userName", "Luigi Eduardo");
                                startActivity(it);
                           // }
                        //}else{
                            //System.out.println("user null");
//                            Log.d("MainActivity", "user null");
                        //}
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}